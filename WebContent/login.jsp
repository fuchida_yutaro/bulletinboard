<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>掲示板</title>
		<link href="./css/login.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<div class="main">

			<h3>ログイン</h3>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="login" method="post">
				<label for="loginId">ログインID</label><br />
				<input name="loginId" value="${ loginId }" id="loginId" /><br />

				<label for="password">パスワード</label><br />
				<input name="password" type="password" id="password" /><br />

				<input type="submit" value="ログイン" /> <br />
			</form>

		</div>
	</body>
</html>