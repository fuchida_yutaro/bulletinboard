package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.CommentService;


@WebServlet("/deleteComment")
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		System.out.println("DeleteComment doPost");

		HttpSession session = request.getSession();

//		数値チェック
		if(!request.getParameter("commentId").matches("^[0-9]{1,10}$")) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("./");
			return;
		}

//		削除するコメントID
		int commentId;
		try {
			commentId = Integer.parseInt(request.getParameter("commentId"));
		} catch (Exception e) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("./");
			return;
		}

//		コメントのユーザーIDとログイン中のユーザーID比較
		CommentService commentService = new CommentService();
		int userId = commentService.getUserId(commentId);
		User loginUser = (User) session.getAttribute("loginUser");

		if(userId != loginUser.getId()) {
			session.setAttribute("errorMessages", "権限がありません");
			response.sendRedirect("./");
			return;
		}

//		コメント削除
		commentService.delete(commentId);

		response.sendRedirect("./");
		System.out.println("");
	}

}
