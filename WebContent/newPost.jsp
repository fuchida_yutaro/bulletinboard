<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>掲示板</title>
		<link href="./css/newPost.css" rel="stylesheet" type="text/css">
	</head>
	<body>

		<div class="header">
			<nav>
				<ul>
					<li><a href="./">ホーム</a></li>
					<li><a href="newPost">新規投稿</a></li>
					<li class="logout"><a href="logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>

		<div class="post-main">
			<h3>新規投稿</h3>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="newPost" method="post">
				<label for="subject">件名</label><br />
				<input name="subject" value="${ post.subject }" id="subject" /><br />

				<label for="text">本文</label><br />
				<textarea name="text">${ post.text }</textarea><br />

				<label for="category">カテゴリー</label><br />
				<input name="category" value="${ post.category }" id="category" /><br />

				<input type="submit" value="投稿" /><br />
			</form>

		</div>

	</body>
</html>