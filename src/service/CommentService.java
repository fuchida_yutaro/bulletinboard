package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import dao.CommentDao;


public class CommentService {

//	コメント登録
	public void register(Comment comment) {

		System.out.println(" CommentService register");

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	全コメント取得
    public List<Comment> getComment() {

    	System.out.println(" CommentService getComment");

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		CommentDao CommentDao = new CommentDao();
    		List<Comment> ret = CommentDao.getComments(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

//  コメント削除
	public void delete(int commentId) {

		System.out.println(" CommentService delete");

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, commentId);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	コメントのユーザーID取得
	public int getUserId(int commentId) {

		System.out.println(" CommentService getUserId");

		Connection connection = null;
		try {
			connection = getConnection();

			int userId = new CommentDao().getUserId(connection, commentId);

			commit(connection);
			return userId;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}