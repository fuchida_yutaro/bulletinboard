
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>掲示板</title>
        <link href="./css/management.css" rel="stylesheet" type="text/css">
    </head>
   <body>
  		<div class="header">
			<nav>
				<ul>
					<li><a href="./">ホーム</a></li>
					<li><a href="newPost">新規投稿</a></li>
					<li><a href="management">ユーザー管理</a></li>
					<li><a href="signup">ユーザー登録</a></li>
					<li class="logout"><a href="logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>

		<div class="main">
			<h3>ユーザー管理</h3>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<table>
				<tr>
					<th>名称</th>
					<th>支店</th>
					<th>部署・役職</th>
					<th>登録日</th>
					<th>ステータス</th>
					<th>変更</th>
					<th>編集</th>
				</tr>

				<c:forEach items="${ users }" var="user">
					<tr>
						<td><c:out value="${ user.name }" /></td>
						<td><c:out value="${ user.branchName }" /></td>
						<td><c:out value="${ user.positionName }" /></td>
						<td><fmt:formatDate value="${ user.createdDate}" pattern="yyyy/MM/dd" /></td>
						<td>
							<c:if test="${user.isWorking == 0}">活動中</c:if>
							<c:if test="${user.isWorking == 1}">停止中</c:if>
						</td>
						<td>
							<c:if test="${ user.id == loginUser.id }">ログイン中</c:if>
							<c:if test="${ user.id != loginUser.id }">
								<form action="editIsWorking" method="post" onSubmit="return check()">
									<input type="hidden" name="userId" value="${ user.id }">
									<c:if test="${user.isWorking == 0}">
										<input type="hidden" name="isWorking" value="1">
										<input type="submit" value="停止" class="status-btn stop-btn">
									</c:if>
									<c:if test="${user.isWorking == 1}">
										<input type="hidden" name="isWorking" value="0">
										<input type="submit" value="復活" class="status-btn restart-btn">
									</c:if>
								</form>
							</c:if>
						</td>
						<td><a href="editUser?userId=${ user.id }">編集</a></td>
					</tr>
				</c:forEach>

			</table>
		</div>

		<script>
			function check(){
				if(window.confirm('本当に変更しますか？')){
					return true;
				} else{
					return false;
				}
			}
		</script>
   </body>
</html>