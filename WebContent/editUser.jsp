<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>掲示板</title>
		<link href="./css/setting.css" rel="stylesheet" type="text/css">
	</head>
	<body>

		<div class="header">
			<nav>
				<ul>
					<li><a href="./">ホーム</a></li>
					<li><a href="newPost">新規投稿</a></li>
					<li><a href="management">ユーザー管理</a></li>
					<li><a href="signup">ユーザー登録</a></li>
					<li class="logout"><a href="logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>

		<div class="main">
			<h3>ユーザー編集</h3>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="editUser" method="post">
				<input name="id" value="${ editUser.id }" id="id" type="hidden"/>

				<label for="loginId">ログインID</label><br />
				<input name="loginId"  value="${ editUser.loginId }" id="loginId" /><br />
				<input type="hidden" name="oldLoginId"  value="${ editUser.loginId }" id="loginId" />

				<label for="newPassword">パスワード</label><br />
				<input name="newPassword" type="password" id="newPassword" /><br />

				<label for="passwordConfirmation">パスワード確認</label><br />
				<input name="passwordConfirmation" type="password" id="passwordConfirmation" /><br />

				<label for="name">名前</label><br />
				<input name="name"  value="${ editUser.name }" id="name" /><br />

				<c:if test="${ editUser.id != loginUser.id }">
					<label>支店</label><br />
					<select name="branchId">
						<c:forEach items="${ branches }" var="branch">
							<option value="${ branch.id }" <c:if test="${ branch.id == editUser.branchId }">selected</c:if>>
								${ branch.name }
							</option>
						</c:forEach>
					</select><br />

					<label>部署・役職</label><br />
					<select name="positionId">
						<c:forEach items="${ positions }" var="position">
							<option value="${ position.id }" <c:if test="${ position.id == editUser.positionId }">selected</c:if>>
								${ position.name }
							</option>
						</c:forEach>
					</select><br />
				</c:if>

				<c:if test="${ editUser.id == loginUser.id }">
					<input name="branchId" value="${ editUser.branchId }" type="hidden"/>
					<input name="positionId" value="${ editUser.positionId }" type="hidden"/>
				</c:if>

				<input type="hidden" name="isWorking" value="${ editUser.isWorking }">

				<input type="submit" value="更新" /><br />
			</form>
		</div>
	</body>
</html>