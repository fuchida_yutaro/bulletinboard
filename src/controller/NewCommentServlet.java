package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		System.out.println("NewComment doPost");

		HttpSession session = request.getSession();
//		エラーメッセージリスト
		List<String> errorMessages = new ArrayList<String>();

		User user = (User) session.getAttribute("loginUser");
		Comment comment = new Comment();
		comment.setText(request.getParameter("comment"));
		comment.setUserId(user.getId());
		try {
			comment.setPostId(Integer.parseInt(request.getParameter("postId")));
		} catch (Exception e) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("./");
			return;
		}

		if (isValid(request, errorMessages) == true) {
			new CommentService().register(comment);
		} else {
			session.setAttribute("errorMessages", errorMessages);
			session.setAttribute("comment", comment);
		}

		response.sendRedirect("./");
		System.out.println("");
	}

//	バリデーション
	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment) == true) {
			errorMessages.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			errorMessages.add("本文は500文字以下で入力してください");
		}
		if (errorMessages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}