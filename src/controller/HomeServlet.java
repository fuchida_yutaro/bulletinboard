package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Post;
import service.CommentService;
import service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		System.out.println("Home doGet");

		request.setCharacterEncoding("UTF-8");

		String category = request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

//		カテゴリー検索 デコード
//		if(StringUtils.isBlank(category) == false) {
//			try {
//				byte[] byteData = category.getBytes("ISO_8859_1");
//				category = new String(byteData, "UTF-8");
//				System.out.println(category);
//			}catch(UnsupportedEncodingException e){
//				System.out.println(e);
//			}
//		}

//		該当する投稿取得
		List<Post> posts = new PostService().getPost(category, startDate, endDate);
//		全コメント取得
		List<Comment> comments = new CommentService().getComment();

		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("searchCategory", category);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);

		request.getRequestDispatcher("home.jsp").forward(request, response);
		System.out.println("");
	}
}