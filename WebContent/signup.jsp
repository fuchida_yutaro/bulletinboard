<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>掲示板</title>
		<link href="./css/setting.css" rel="stylesheet" type="text/css">
	</head>
	<body>

		<div class="header">
			<nav>
				<ul>
					<li><a href="./">ホーム</a></li>
					<li><a href="newPost">新規投稿</a></li>
						<li><a href="management">ユーザー管理</a></li>
						<li><a href="signup">ユーザー登録</a></li>
					<li class="logout"><a href="logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>

		<div class="main">
			<h3>ユーザー新規登録</h3>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="signup" method="post">
				<label for="loginId">ログインID</label><br />
				<input name="loginId"  value="${ user.loginId }" id="loginId" /><br />

				<label for="password">パスワード</label><br />
				<input name="password" type="password" id="password" /><br />

				<label for="passwordConfirmation">パスワード確認</label><br />
				<input name="passwordConfirmation" type="password" id="passwordConfirmation" /><br />

				<label for="name">名前</label><br />
				<input name="name"  value="${ user.name }" id="name" /><br />

				<label>支店</label><br />
				<select name="branchId">
					<c:forEach items="${ branches }" var="branch">
						<option value="${ branch.id }" <c:if test="${ branch.id == user.branchId }">selected</c:if>>
							${ branch.name }
						</option>
					</c:forEach>
				</select><br />

				<label>部署・役職</label><br />
				<select name="positionId">
					<c:forEach items="${ positions }" var="position">
						<option value="${ position.id }" <c:if test="${ position.id == user.positionId }">selected</c:if>>
							${ position.name }
						</option>
					</c:forEach>
				</select><br />

				<input type="hidden" name="isWorking" value="0">

				<input type="submit" value="登録" /><br />
			</form>
		</div>


	</body>
</html>