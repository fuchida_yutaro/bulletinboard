package controller;

import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.PostService;


@WebServlet("/deletePost")
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		System.out.println("DeletePost doPost");

		HttpSession session = request.getSession();

//		数値チェック
		if(!request.getParameter("postId").matches("^[0-9]*$")) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("./");
			return;
		}

//		削除するpostId
		int postId;
		try {
			postId = Integer.parseInt(request.getParameter("postId"));
		} catch (Exception e) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("./");
			return;
		}

//		投稿のユーザーIDとログイン中のユーザーID比較
		PostService postService = new PostService();
		int userId = postService.getUserId(postId);
		User loginUser = (User) session.getAttribute("loginUser");

		if(userId != loginUser.getId()) {
			session.setAttribute("errorMessages", "権限がありません");
			response.sendRedirect("./");
			return;
		}

//		投稿削除
		postService.delete(postId);

		response.sendRedirect("./");
		System.out.println("");
	}

}
