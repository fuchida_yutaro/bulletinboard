<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>掲示板</title>
		<link href="./css/home.css" rel="stylesheet" type="text/css">
	</head>

	<body>

		<div class="header">
			<nav>
				<ul>
					<li><a href="./">ホーム</a></li>
					<li><a href="newPost">新規投稿</a></li>
					<li><a href="management">ユーザー管理</a></li>
					<li class="logout"><a href="logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<h2>掲示板</h2>
		<div class="search-form">
			<form action="" method="get">

				<label for="category">カテゴリー：</label>
				<input name="category" value="${ searchCategory }" id="category" />

				<label>開始日：</label>
				<input type="date" name="startDate" value="${ startDate }">

				<label>終了日：</label>
				<input type="date" name="endDate" value="${ endDate }">

				<input type="submit" value="検索" class="search-btn" />
			</form>
		</div>

		<div class="main">
			<div class="search-result">
				<p>${ posts.size() } 件</p>
			</div>

			<c:forEach items="${ posts }" var="post">

				<div class="post">
					<div class="post-area">
						<h4><c:out value="${ post.subject }" /></h4>
						<div class="name">-　<c:out value="${ post.userName }" /></div>
						<div class="text">
							<p><c:out value="${ post.text }" /></p>
						</div>
						<div class="post-info category">カテゴリー：<c:out value="${ post.category }" /></div>
						<div class="post-info date"><fmt:formatDate value="${ post.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>

						<c:if test="${ post.userId == loginUser.id }">
							<form action="deletePost" method="post" onSubmit="return check()" id="post-form">
								<input type="hidden" name="postId" value="${ post.id }">
								<input type="submit" value="削除" class="delete-btn">
							</form>
						</c:if>
						<div class="post-border"></div>
					</div>

					<div class="comment-area">
						<c:forEach items="${ comments }" var="comment">
							<c:if test="${ comment.postId == post.id }">
								<div class="comment">
									<div class="comment-text">
										<p><c:out value="${ comment.text }" /></p>
									</div>
									<div class="commnet-info">
										<span class="comment-name">名前：<c:out value="${ comment.userName }" /></span>
										<span class="date">
											<fmt:formatDate value="${ comment.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" />
										</span>
									</div>

									<c:if test="${ comment.userId == loginUser.id }">
										<form action="deleteComment" method="post" onSubmit="return check()" id="comment-form">
											<input type="hidden" name="commentId" value="${ comment.id }">
											<input type="submit" value="削除" class="delete-btn">
										</form>
									</c:if>
								</div>
								<div class="comment-border"></div>
							</c:if>
						</c:forEach>
					</div>

					<div class="comment-form">
						<form action="newComment" method="post">
							<textarea name="comment" class=""  cols="100">
								<c:if test="${ comment.postId == post.id }">
									<c:out value="${ comment.text }" />
									<c:remove var="comment" scope="session"/>
								</c:if>
							</textarea>
							<input type="hidden" name="postId" value="${ post.id }"><br />
							<input type="submit" value="コメントする" class="comment-btn">

						</form>
					</div>

				</div>

			</c:forEach>
		</div>

		<script>
			function check(){
				if(window.confirm('本当に削除しますか？')){
					return true;
				} else{
					return false;
				}
			}
		</script>

	</body>
</html>