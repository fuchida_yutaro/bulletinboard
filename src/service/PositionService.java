package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Position;
import dao.PositionDao;

public class PositionService {
//	全部署・役職名取得
    public List<Position> getPosition() {

    	System.out.println(" PositionService getPosition");

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		PositionDao positionDao = new PositionDao();
    		List<Position> ret = positionDao.getPositions(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}
