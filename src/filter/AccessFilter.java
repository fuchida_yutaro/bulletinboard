package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class AccessFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException{

		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session = ((HttpServletRequest)request).getSession();

		String servletName = ((HttpServletRequest)request).getServletPath();

		User loginUser = (User) session.getAttribute("loginUser");

//		ログイン確認
		if(loginUser == null && !servletName.equals("/login") && !servletName.equals("/css/login.css")){
			session.setAttribute("errorMessages", "ログインしてください");
			httpResponse.sendRedirect("login");

//		権限確認
		}else if(servletName.equals("/management") || servletName.equals("/signup") || servletName.equals("/editUser")) {

			if (loginUser.getPositionId() == 1){
				chain.doFilter(request, response);
			} else {
				session.setAttribute("errorMessages", "権限がありません");
				httpResponse.sendRedirect("./");
			}

		} else {
			chain.doFilter(request, response);
		}

	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}
