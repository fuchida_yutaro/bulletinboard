package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		System.out.println("SignUp doGet");

//		セレクトボタンの入力値
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();

		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("signup.jsp").forward(request, response);

		System.out.println("");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		System.out.println("SignUp doPost");

		HttpSession session = request.getSession();

		List<String> errorMessages = new ArrayList<String>();

//		入力値をセット
		User user = new User();
		user.setLoginId(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		try {
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
			user.setIsWorking(Integer.parseInt(request.getParameter("isWorking")));
		} catch (Exception e) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("signup");
			return;
		}

		UserService userService = new UserService();

//		ログインID重複
		boolean loginIdValid = userService.loginIdCheck(user.getLoginId());
		if(loginIdValid == false) {
			errorMessages.add("このログインIDは、すでに使われています");
		}
//		入力値チェック
		boolean isValid = userService.isValid(request, errorMessages, true);

		if (isValid && loginIdValid) {
			userService.register(user);
			response.sendRedirect("management");
		} else {
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
		System.out.println("");
	}
}