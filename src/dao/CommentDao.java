package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

//	コメント INSERT
	public void insert(Connection connection, Comment comment) {

		System.out.println("  CommentDao insert");

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("text");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // text
			sql.append(", ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	コメント DELETE (commentId)
	public void delete(Connection connection, int commentId) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM comments WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentId);

//			System.out.println(ps.toString());

			int deleteCount = ps.executeUpdate();

			if (deleteCount == 1) {
				System.out.println("  CommentDao delete");
			} else {
				System.out.println("削除失敗");
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	コメントのユーザーID SELECT (commentId)
	public int getUserId(Connection connection, int commentId) {

		System.out.println("   CommentDao getUserId");

		PreparedStatement ps = null;
		try {
			String sql = "SELECT user_id FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentId);

//			System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();

			int userId = 0;
			while (rs.next()) {
				userId = rs.getInt("user_id");
			}
			return userId;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	全コメント SELECT
	public List<Comment> getComments(Connection connection) {

		System.out.println("  CommentDao getComments");

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("users.name as name, ");
			sql.append("comments.text as text, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.post_id as post_id, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	コメントリスト化
	private List<Comment> toCommentList(ResultSet rs)
	        throws SQLException {

		System.out.println("   CommentDao toCommentList");

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				int postId = rs.getInt("post_id");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setUserName(name);
				comment.setText(text);
				comment.setUserId(userId);
				comment.setPostId(postId);
				comment.setCreatedDate(createdDate);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
			}
		}

}