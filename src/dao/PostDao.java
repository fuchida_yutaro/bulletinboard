package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import exception.SQLRuntimeException;

public class PostDao {

//	投稿 INSERT
	public void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
//			SQL文宣言
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // subject
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", ?"); // user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

//			値をセット
			ps.setString(1, post.getSubject());
			ps.setString(2, post.getText());
			ps.setString(3, post.getCategory());
			ps.setInt(4, post.getUserId());

			ps.executeUpdate();
			System.out.println("  PostDao insert");

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	投稿 DELETE (postId)
	public void delete(Connection connection, int postId) {
		PreparedStatement ps = null;
		try {
//			SQL文の宣言
			String sql = "DELETE FROM posts WHERE id = ?";
//			プリコンパイルされたSQL文を表すオブジェクトの取得
			ps = connection.prepareStatement(sql);
//			バインド変数に値を設定
			ps.setInt(1, postId);

			int deleteCount = ps.executeUpdate();

			if (deleteCount == 1) {
				System.out.println("  PostDao delete");
			} else {
				System.out.println("削除失敗");
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	投稿のユーザーID SELECT (postId)
	public int getUserId(Connection connection, int postId) {

		System.out.println("   PostDao getUserId");

		PreparedStatement ps = null;
		try {
			String sql = "SELECT user_id FROM posts WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, postId);

			ResultSet rs = ps.executeQuery();

//			System.out.println(ps.toString());

			int userId = 0;
			while (rs.next()) {
				userId = rs.getInt("user_id");
			}
			return userId;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	投稿 SELECT
	public List<Post> getPosts(Connection connection, String category, Date startDate, Date endDate) {

		System.out.println("  PostDao getPosts");

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("users.name as name, ");
			sql.append("posts.subject as subject, ");
			sql.append("posts.text as text, ");
			sql.append("posts.category as category, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("posts.created_date as created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE posts.created_date >= ? AND posts.created_date < ? ");
			if(StringUtils.isBlank(category) == false) {
				sql.append("AND posts.category LIKE ?");
			}
			sql.append("ORDER BY created_date DESC ");

			ps = connection.prepareStatement(sql.toString());

//			値をセット
			ps.setTimestamp(1, new Timestamp(startDate.getTime()));
			ps.setTimestamp(2, new Timestamp(endDate.getTime()));
			if(StringUtils.isBlank(category) == false) {
				ps.setString(3, "%" + category + "%");
			}

			System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
			List<Post> ret = toPostList(rs);

			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	全投稿リスト化
	private List<Post> toPostList(ResultSet rs) throws SQLException {

		System.out.println("   PostDao toPostList");

		List<Post> ret = new ArrayList<Post>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Post post = new Post();
				post.setId(id);
				post.setUserName(name);
				post.setSubject(subject);
				post.setText(text);
				post.setCategory(category);
				post.setUserId(userId);
				post.setCreatedDate(createdDate);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}