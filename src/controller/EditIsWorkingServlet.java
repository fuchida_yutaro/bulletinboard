package controller;

import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/editIsWorking" })
public class EditIsWorkingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		System.out.println("EditIsWorking doPost");

		HttpSession session = request.getSession();

//		数値チェック
		boolean userIdFormat = !request.getParameter("userId").matches("^[0-9]{1,10}$");
		boolean isWorkingFormat = !request.getParameter("isWorking").matches("^[0-1]{1}$");
		if(userIdFormat || isWorkingFormat) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("management");
			return;
		}

//		更新するユーザーID、ステータス
		int userId;
		try {
			userId = Integer.parseInt(request.getParameter("userId"));
		} catch (Exception e) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("management");
			return;
		}
		int isWorking = Integer.parseInt(request.getParameter("isWorking"));

//		ユーザーの存在チェック
		User user = new UserService().getUser(userId);
		if(user == null) {
			session.setAttribute("errorMessages", "ユーザーが存在しません");
			response.sendRedirect("management");
			return;
		}

//		ステータス更新
		new UserService().isWorkingUpdate(userId, isWorking);

		response.sendRedirect("management");
		System.out.println("");
	}

}
