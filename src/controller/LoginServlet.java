package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		System.out.println("Login doGet");

		HttpSession session = ((HttpServletRequest)request).getSession();

//		ログイン済みチェック
		if(session.getAttribute("loginUser") != null){
			session.setAttribute("errorMessages", "ログイン済みです");
			response.sendRedirect("./");
		} else {
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
		System.out.println("");
	}

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {

		System.out.println("Login doPost");

		HttpSession session = request.getSession();

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

////		空白チェック
//		if(StringUtils.isBlank(loginId) || StringUtils.isBlank(password) ) {
//			session.setAttribute("errorMessages", "ログインIDまたはパスワードが入力されていません");
//			response.sendRedirect("login");
//			return;
//		}

//		ログインユーザー情報取得
		UserService loginService = new UserService();
		User user = loginService.login(loginId, password);

		if (user != null) {

//			アカウント停止中
			if(user.getIsWorking() != 0) {
				request.setAttribute("errorMessages", "ログインIDまたはパスワードが誤っています（停止）");
				request.setAttribute("loginId", loginId);
				request.getRequestDispatcher("login.jsp").forward(request, response);

			} else {
//				セッションにログインユーザーの情報をセット
				session.setAttribute("loginUser", user);
				session.removeAttribute("errorMessages");
				response.sendRedirect("./");
			}

		} else {
			request.setAttribute("errorMessages", "ログインIDまたはパスワードが誤っています");
			request.setAttribute("loginId", loginId);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
		System.out.println("");
	}

}