package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;

public class BranchService {

//	全支店名取得
    public List<Branch> getBranch() {

    	System.out.println(" BranchService getBranch");

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		BranchDao branchDao = new BranchDao();
    		List<Branch> ret = branchDao.getBranches(connection);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}
