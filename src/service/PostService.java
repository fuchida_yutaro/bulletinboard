package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import dao.PostDao;

public class PostService {

//	投稿登録
	public void register(Post post) {

		System.out.println(" PostService register");

		Connection connection = null;
		try {
//			DBコネクションを取得
			connection = getConnection();

//			SQL文作成、実行
			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	投稿取得数 最大値
//	private static final int LIMIT_NUM = 1000;

//	投稿取得
    public List<Post> getPost(String category, String startDate, String endDate) {

    	System.out.println(" PostService getPost");

//		日付フォーマット
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");

//		デフォルト開始日・終了日
		Date startTime = new GregorianCalendar(2018, 0, 1, 0, 0, 0).getTime();
		Date endTime = new Date();

//		期間絞り込み時
		try {
			if(StringUtils.isBlank(startDate) == false) {
//				開始日をセット
				startTime = timeFormat.parse(startDate);
			}
			if(StringUtils.isBlank(endDate) == false) {
//				終了日の１日後をセット
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(timeFormat.parse(endDate));
				calendar.add(Calendar.DAY_OF_MONTH, 1);
				endTime = calendar.getTime();
			}
		} catch (ParseException ex) {
			ex.printStackTrace();
		}

    	Connection connection = null;
    	try {
    		connection = getConnection();

//    		投稿取得
    		PostDao postDao = new PostDao();
    		List<Post> ret = postDao.getPosts(connection, category, startTime, endTime);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

//    投稿削除
    public void delete(int postId) {

    	System.out.println(" PostService delete");

		Connection connection = null;
		try {
//			DBコネクションを取得
			connection = getConnection();

//			SQL文作成、実行
			PostDao postDao = new PostDao();
			postDao.delete(connection, postId);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	投稿のユーザーID取得
	public int getUserId(int postId) {
		System.out.println(" PostService getUserId");

		Connection connection = null;
		try {
			connection = getConnection();

			int userId = new PostDao().getUserId(connection, postId);

			commit(connection);
			return userId;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}