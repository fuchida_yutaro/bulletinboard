package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/editUser" })
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {

		System.out.println("EditUser doGet");

		HttpSession session = request.getSession();

//		数値チェック
		if(!request.getParameter("userId").matches("^[0-9]{1,10}$")) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("management");
			return;
		}

		int userId;
		try {
			userId = Integer.parseInt(request.getParameter("userId"));
		} catch (Exception e) {
			session.setAttribute("errorMessages", "エラーが発生しました");
			response.sendRedirect("management");
			return;
		}

//		DBからユーザー情報取得
		User editUser = new UserService().getUser(userId);
//		ユーザーの存在チェック
		if(editUser == null) {
			session.setAttribute("errorMessages", "ユーザーが存在しません");
			response.sendRedirect("management");
			return;
		}

//		セレクトボタンの値
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();

		request.setAttribute("editUser", editUser);
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("editUser.jsp").forward(request, response);

		System.out.println("");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		System.out.println("EditUser doPost");

		List<String> errorMessages = new ArrayList<String>();

//		パスワードの変更の有無
		boolean changePassword = false;
		if (StringUtils.isNotBlank(request.getParameter("newPassword")) == true) {
			changePassword = true;
		}

//		入力データセット
		User editUser = getEditUser(request, changePassword);

		UserService userService = new UserService();

//		変更するユーザーのログインID
		String oldLoginId = userService.getUser(editUser.getId()).getLoginId();

//		ログインIDの変更の有無
		boolean loginIdValid = true;

		if(!editUser.getLoginId().equals(oldLoginId)) {
			loginIdValid = userService.loginIdCheck(editUser.getLoginId());
//			ログインID重複
			if(loginIdValid == false) {
				errorMessages.add("このログインIDは、すでに使われています");
			}
		}
//		入力チェック
		boolean isValid = userService.isValid(request, errorMessages, false);

		if (isValid && loginIdValid) {

//			更新
			userService.update(editUser, changePassword);

//			ログイン中のユーザー情報更新
			HttpSession session = request.getSession();
			User loginUser = (User) session.getAttribute("loginUser");
			if(editUser.getId() == loginUser.getId() ) {
				session.setAttribute("loginUser", editUser);
			}
			response.sendRedirect("management");
		} else {
//			セレクトボタンの値
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();

			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("editUser.jsp").forward(request, response);
		}
		System.out.println("");
	}

//	ユーザ情報セット
	private User getEditUser(HttpServletRequest request, boolean changePassword)
			throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		if(changePassword) {
			editUser.setPassword(request.getParameter("newPassword"));
		}
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		editUser.setIsWorking(Integer.parseInt(request.getParameter("isWorking")));

		return editUser;
	}

}