package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

//	全支店取 SELECT
	public List<Branch> getBranches(Connection connection) {

		System.out.println("  BranchDao getBranches");

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//	リスト化
	private List<Branch> toBranchList(ResultSet rs)
	        throws SQLException {

		System.out.println("   BranchDao toBranchList");

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);
				branch.setCreatedDate(createdDate);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
			}
		}

	}