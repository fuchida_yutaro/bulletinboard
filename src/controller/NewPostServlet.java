package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("newPost.jsp").forward(request, response);
		System.out.println("NewPost doGet");
		System.out.println("");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		System.out.println("NewPost doPost");

		HttpSession session = request.getSession();

		List<String> errorMessages = new ArrayList<String>();

//		postインスタンスに入力値をセット
		Post post = new Post();
		post.setSubject(request.getParameter("subject"));
		post.setText(request.getParameter("text"));
		post.setCategory(request.getParameter("category"));

		if (isValid(request, errorMessages) == true) {

//			ログイン中のユーザー情報
			User user = (User) session.getAttribute("loginUser");
			post.setUserId(user.getId());

//			DBに保存
			new PostService().register(post);

			response.sendRedirect("./");

		} else {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("post", post);
			request.getRequestDispatcher("newPost.jsp").forward(request, response);
		}
		System.out.println("");
	}

//	バリデーション
	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		validation("件名", subject, 30, errorMessages);
		validation("本文", text, 1000, errorMessages);
		validation("カテゴリ", category, 10, errorMessages);

		if (errorMessages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

//	空欄、文字数チェック
	public void validation(String title, String text, int maximum, List<String> errorMessages) {
		if (StringUtils.isBlank(text) == true) {
			errorMessages.add(title + "を入力してください");
		}
		if (maximum < text.length()) {
			errorMessages.add(title + "は" + maximum + "文字以下で入力してください");
		}
	}

}