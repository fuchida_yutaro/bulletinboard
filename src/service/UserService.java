package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

//	ユーザー登録
	public void register(User user) {

		System.out.println(" UserService register");

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	ログイン
	public User login(String loginId, String password) {

		System.out.println(" UserService login");

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();

//			パスワード暗号化
			String encPassword = CipherUtil.encrypt(password);

//			ログインユーザー情報取得
			User user = userDao.getUser(connection, loginId, encPassword);
			commit(connection);
			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	全ユーザー取得
	public List<User> getUsers() {

    	System.out.println(" UserService getUsers");

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		List<User> ret = userDao.getUsers(connection);

    		commit(connection);
    		return ret;

    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

//	ユーザー情報取得
    public User getUser(int userId) {

    	System.out.println(" UserService getUser");

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User user = userDao.getUser(connection, userId);

    		commit(connection);
    		return user;

    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }


//    ユーザー情報更新
    public void update(User user, boolean changePassword) {

    	System.out.println(" UserService update");
		Connection connection = null;
		try {
			connection = getConnection();

			if(changePassword) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();

			userDao.update(connection, user, changePassword);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//  ユーザーステータス更新
    public void isWorkingUpdate(int userId, int isWorking) {

    	System.out.println(" UserService isWorkingUpdate");

		Connection connection = null;
		try {
//			DBコネクションを取得
			connection = getConnection();

//			SQL文作成、更新
			UserDao userDao = new UserDao();
			userDao.isWorkingUpdate(connection, userId, isWorking);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


//    入力チェック（登録、更新）
    public boolean isValid(HttpServletRequest request, List<String> errorMessages, boolean register) {

//		ログインID
		String loginId = request.getParameter("loginId");
//		空欄チェック
		if (StringUtils.isBlank(loginId) == true) {
			errorMessages.add("ログインIDを入力してください");
		} else {

//			パターン・長さチェック
			int idLength = loginId.length();
			if(!Pattern.matches("^[\\w]+$", loginId) || idLength < 6 || idLength > 20) {
				errorMessages.add("ログインIDは、半角英数字の6文字以上20文字以下で入力してください");
			}
		}

//		パスワード登録
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("passwordConfirmation");

//		パスワード更新
		String newPassword = request.getParameter("newPassword");
		if(StringUtils.isNotBlank(newPassword) == true) {
			password = newPassword;
		}

//		登録または、パスワード更新
		if(register || StringUtils.isNotBlank(newPassword) == true) {

//			空欄チェック
			if (StringUtils.isBlank(password) == true) {
				errorMessages.add("パスワードを入力してください");
			} else {

//				パターン・長さチェック
				int passwordLength = password.length();
				if(!Pattern.matches("[!-_@+*;:#$%&\\w]+", password) || passwordLength < 6 || passwordLength > 20) {
					errorMessages.add("パスワードは、記号を含む半角文字の6文字以上20文字以下で入力してください");
				}
//				パスワード一致チェック
				if (!password.equals(passwordConfirmation)) {
					errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
				}
			}
		}

//		名前
		String name = request.getParameter("name");
//		空欄チェック
		if (StringUtils.isBlank(name) == true) {
			errorMessages.add("ユーザー名を入力してください");
		}
//		長さチェック
		if(name.length() > 10) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}

//		支店-部署・役職
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));
//		組み合わせチェック
		if(branchId == 1) {
			if (!(positionId == 1 || positionId == 2)) {
				errorMessages.add("支店と部署・役職の組み合わせが不正です");
			}
		} else {
			if (positionId == 1 || positionId == 2) {
				errorMessages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		if (errorMessages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

//	ログインID重複チェック
    public boolean loginIdCheck(String loginId) {

    	System.out.println(" UserService loginIdCheck");

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		boolean loginIdCheck = userDao.loginIdCheck(connection, loginId);

    		commit(connection);
    		return loginIdCheck;

    	} catch (RuntimeException e) {
    		rollback(connection);
    		return false;
    	} catch (Error e) {
    		rollback(connection);
    		return false;
    	} finally {
    		close(connection);
    	}
    }
}